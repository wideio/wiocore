from django.conf.urls import *
from aiosciweb1.lib import utilviews

import views

urlpatterns=patterns('docs.views',
         *utilviews.autourls(views)
)
