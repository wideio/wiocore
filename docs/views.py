# Create your views here.
from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, RequestContext
from django.contrib import messages
from django.db.models.fields.related import ForeignKey
from pygments import highlight
import json
from django.http import HttpResponse

#from aiosciweb.compute import Token

import settings
from docutils.core import publish_parts

from django.db.models.fields.files import FileField


def help_modules(request):
    """
    This view return the list of WIDE IO applications and explain succinctly what they do
    """
    LA=[]
    
    blacklist = ['countries','backoffice','socialnetwork','payment','django_cron','monitoring','registration','wioregistration','challenges','wiocore','evaluation']
    
    for a in settings.INSTALLED_APPS:
        for b in blacklist:
          if a.startswith(b):
            a=""
            break
        if (a):
            try:
                m=__import__(a+".models",fromlist=a.split("."))
                if (hasattr(m,"MODELS")):
                    m2=__import__(a,fromlist=a.split(".")[:-1])
                    docstr=m2.__doc__
                    if docstr==None:
                        docstr=""
                    LA.append((m.__name__.split('.')[0]  ,m, publish_parts(docstr,writer_name='html')['body']))

            except ImportError,e:
                print a,e
    context={
        'modules':LA,
        'request':request,
    }
    return render_to_response("help/modules.html", context, RequestContext(request))
help_modules.default_url='modules'

def help_models(request, modulename):
    """
    This view return the list of WIDE IO applications' models and explain succinctly what they do and what their fields are.
    """
    LM=[]
    from pygments.lexers import PythonLexer,BashLexer,JsonLexer
    from pygments.formatters import HtmlFormatter    
    try:
        models = __import__(modulename+".models",fromlist=[str(modulename)])
        m2=__import__(modulename,fromlist=modulename.split(".")[:-1])
        docstr2=m2.__doc__
        if docstr2==None:
            docstr2=""
        else:
            docstr2=publish_parts(docstr2,writer_name='html')['body']
        for model in models.MODELS:
            if not hasattr(model, "can_list") or not hasattr(model, "can_add"):
                continue

            if (not model.can_list(request) and not model.can_add(request)):
                continue

            docstr=model.__doc__

            if docstr==None:
                docstr=""

            if (model!=None):
                docs = []
                fields = []
                all_fields = []
                python_filefields = []
                filefields = []
                audlv = {}
                try:
                    for f in model._meta.fields:
                        if type(f) == ForeignKey:
                            all_fields.append(f.name+"_id")
                        else:
                            all_fields.append(f.name)
                        if type(f) == FileField:
                            filefields.append(f.name)
                            python_filefields.append("i_"+f.name)
                        elif f.name not in model.WIDEIO_Meta.form_exclude:
                            fields.append(f.name)
                    # CURL
                    if len(filefields) == 0:
                        audlv.update({"add":"curl -u "+request.user.username+":password " + " -F _JSON=1 -F " + "-F ".join([e+"=value " for e in fields]) + " " + settings.SITE_URL[:-1]+model.get_base_url() + "add/"})
                    else:
                        audlv.update({"add":"curl -u "+request.user.username+":password " + " -F _JSON=1 -F " + "-F ".join([e+"=value " for e in fields]) + " -F " + "-F ".join([e+"=@path " for e in filefields]) + " " + settings.SITE_URL[:-1]+model.get_base_url() + "add/"})

                    audlv["add"] = highlight(audlv["add"], BashLexer(), HtmlFormatter())
                    
                    audlv.update({"view":"curl -u "+request.user.username+":password -F _JSON=1 " + settings.SITE_URL[:-1]+model.get_base_url() + "view/ID/"})
                    audlv["view"] = '<pre data-lang="shell">'+audlv["view"]+'</pre>'
                    #audlv["view"] = highlight(audlv["view"], BashLexer(), HtmlFormatter()) #old pygments
                    
                    audlv.update({"list":"curl -u "+request.user.username+":password -F _JSON=1 " + settings.SITE_URL[:-1]+model.get_base_url() + "list/"})
                    audlv["list"] = '<pre data-lang="shell">'+audlv["list"]+'</pre>'
                    #audlv["list"] = highlight(audlv["list"], BashLexer(), HtmlFormatter())
                    
                    audlv.update({"del":"curl -u "+request.user.username+":password -F _JSON=1 " + settings.SITE_URL[:-1]+model.get_base_url() + "del/ID/"})
                    audlv["del"] = '<pre data-lang="shell">'+audlv["del"]+'</pre>'
                    #audlv["del"] = highlight(audlv["del"], BashLexer(), HtmlFormatter())

                    audlv.update({"isotherviews":False})
                    if hasattr(model.WIDEIO_Meta,"other_views"):
			audlv["isotherviews"] = True
	                audlv.update({"otherviews":[]})
			for view in model.WIDEIO_Meta.other_views:
				f = map(lambda x:x[0],view["inputs"])
	               		audlv["otherviews"].append((view["name"],view["doc"],'<pre data-lang="shell">curl -u '+request.user.username+':password -F _JSON=1 -F '  + "-F ".join([e+"=value " for e in f]) + " " + settings.SITE_URL[:-1]+view["url"]+'</pre>'))

                    docs.append(("CURL API",audlv))
                    
                    audlv = {}
                    #PYTHON
                    name = model.__name__
                    if len(filefields) == 0:
                        audlv.update({"add":">>> wideio." + name.lower()+".add(" + ", ".join([e+"=value" for e in fields]) +")"})
                    else:
                        audlv.update({"add":">>> wideio." + name.lower()+".add(" + ", ".join([e+"=value" for e in fields]) + ", " + ", ".join([e+"=@path" for e in python_filefields]) +")"})
                        
                    audlv["add"] = '<pre data-lang="python">'+audlv["add"]+'</pre>'
                    #audlv["add"] = highlight(audlv["add"], PythonLexer(), HtmlFormatter())
                    
                    audlv.update({"view":">>> wideio." + name.lower()+".view(ID)"})
                    audlv["view"] = '<pre data-lang="python">'+audlv["view"]+'</pre>'
                    #audlv["view"] = highlight(audlv["view"], PythonLexer(), HtmlFormatter())
                    
                    audlv.update({"list":">>> wideio." + name.lower()+".all()"})
                    audlv["list"] = '<pre data-lang="python">'+audlv["list"]+'</pre>'
                    #audlv["list"] = highlight(audlv["list"], PythonLexer(), HtmlFormatter())
                    
                    audlv.update({"del":">>> wideio." + name.lower()+".delete(ID)"})
                    audlv["del"] = '<pre data-lang="python">'+audlv["del"]+'</pre>'
                    #audlv["del"] = highlight(audlv["del"], PythonLexer(), HtmlFormatter())

                    audlv.update({"isotherviews":False})
                    if hasattr(model.WIDEIO_Meta,"other_views"):
			audlv["isotherviews"] = True
	                audlv.update({"otherviews":[]})
			for view in model.WIDEIO_Meta.other_views:
				f = map(lambda x:x[0],view["inputs"])
	               		audlv["otherviews"].append((view["name"],view["doc"],'<pre data-lang="python">>>> wideio.' + name.lower() + '.' + view["name"]+'(' + ", ".join([e+"=value" for e in f]) + ')</pre>'))

                    docs.append(("Python API",audlv))

                    audlv = {}

                    # RESULT
                    res = {"res":"ID"}
                    documentation = []
                    if hasattr(model.WIDEIO_Meta,"on_add_return_doc"):
                        for key,doc,ex in model.WIDEIO_Meta.on_add_return_doc:
                            res.update({key:ex})
                            documentation.append((key,doc))

                    audlv.update({"add":json.dumps(res, sort_keys=True,indent=4, separators=(',', ': '))})
                    #audlv["add"] = highlight(audlv["add"].replace(',',',\n'), JsonLexer(), HtmlFormatter())
                    audlv["add"] = '<pre data-lang="javascript">'+audlv["add"]+'</pre>'
                    audlv.update({"add_doc":documentation})

                    res = {}
                    documentation = []
                    if hasattr(model.WIDEIO_Meta,"on_view_return_doc"):
                        for key,doc,ex in model.WIDEIO_Meta.on_view_return_doc:
                            res.update({key:ex})
                            documentation.append((key,doc))

                    res.update(dict(map(lambda x:(x,"value"),all_fields)))

                    audlv.update({"view":json.dumps(res, sort_keys=True,indent=4, separators=(',', ': '))})
                    audlv["view"] = '<pre data-lang="javascript">'+audlv["view"]+'</pre>'
                    #audlv["view"] = highlight(audlv["view"].replace(',',',\n'), JsonLexer(), HtmlFormatter())
                    audlv.update({"view_doc":documentation})


                    audlv.update({"del":json.dumps({}, sort_keys=True,indent=4, separators=(',', ': '))})
                    audlv["del"] = '<pre data-lang="javascript">'+audlv["del"]+'</pre>'
                    #audlv["del"] = highlight(audlv["del"].replace(',',',\n'), JsonLexer(), HtmlFormatter())


                    audlv.update({"isotherviews":False})
                    if hasattr(model.WIDEIO_Meta,"other_views"):
			audlv["isotherviews"] = True
	                audlv.update({"otherviews":[]})
			for view in model.WIDEIO_Meta.other_views:
				try:
					res = json.dumps({view["output"]}, sort_keys=True,indent=4, separators=(',', ': '))
		               		audlv["otherviews"].append((view["name"],view["doc"],'<pre>'+res+'</pre>'))
				except:
		               		audlv["otherviews"].append((view["name"],view["doc"],'<pre>' + view["output"] + '</pre>'))
                    docs.append(("Results",audlv))
                    #audlv.update()
                    LM.append((model.__name__, model, publish_parts(docstr,writer_name='html')['body'],docs[:-1],docs[-1]))
                except Exception as e:
                    print "Exception",e
                
    
        context={
            'module':modulename,
            'helpmod':docstr2,
            'models':LM,
            'request':request,
            'subreqparam':{'AJAX':1,'JSON':1}
        }

        return render_to_response("help/models.html", context, RequestContext(request))
    except Exception,e:
        if  not request.user.is_staff:
          messages.add_message(request, messages.WARNING, "Error in this module")
          return HttpResponseRedirect('../')
        else:
          raise
help_models.default_url='modules/([^/]+)'

from aiosciweb1.lib.wiotext import _wiotext
from aiosciweb1.lib.decorators import user_passes_test

def dedent(t):
  t=t.split("\n")
  l=filter(lambda x:len(x.strip()),t)
  c=0
  cr=l[0]
  while (len(cr)-c>0) and (cr[c].isspace()):
    c+=1
  return "\n".join(map(lambda x:x[c:],t))

@user_passes_test(lambda u:u.is_staff)
def internal_help(request, module, lib, element):
  doc=getattr(__import__(module+"."+lib,fromlist=[module]),element).__doc__
  page_extrahead=""
  t="# WIDE IO - Internal documentation\n"
  t+="## %s > %s >  %s \n" %(module, lib,element)
  t+=dedent(doc)
  context={'page_extrahead':page_extrahead,'thecontent':_wiotext(t,request)}
  return render_to_response("basic.html",context,RequestContext(request))


internal_help.default_url='internal_help/([^/]+)/([^/]+)/([^/]+)'  

VIEWS= [ help_modules, help_models , internal_help]

