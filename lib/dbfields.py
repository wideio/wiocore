#from __future__ import unicode_literals

#import copy
#import datetime
#import decimal
#import math
#import warnings
#from itertools import tee

#from django.db import connection
#from django.db.models.query_utils import QueryWrapper
#from django.conf import settings
#from django import forms
#from django.core import exceptions, validators
#from django.utils.datastructures import DictWrapper
#from django.utils.dateparse import parse_date, parse_datetime, parse_time
#from django.utils.functional import curry, total_ordering
#from django.utils.itercompat import is_iterator
#from django.utils.text import capfirst
#from django.utils import timezone
#from django.utils.translation import ugettext_lazy as _
#from django.utils.encoding import smart_text, force_text
#from django.utils.ipv6 import clean_ipv6_address
#from django.utils import six

from django.core import exceptions, validators
from django.utils.translation import ugettext_lazy as _
import django.db.models as models

from django.db.models import *

default_error_messages = {
    'invalid_choice': _('Value %r is not a valid choice.'),
    'null': _('This field cannot be null.'),
    'blank': _('This field cannot be blank.'),
    'unique': _('%(model_name)s with this %(field_label)s '
		'already exists.'),
    'app_null': _('This field cannot be null'),
    'db_null': _('This field cannot be null'),
    'app_blank	': _('This field cannot be blank'),
    'db_blank': _('This field cannot be blank'),
}
models.Field.default_error_messages = default_error_messages

#class Field(models.Field):
#     def __init__(self, *args, **kwargs):
#	self.app_p = dict(map(lambda i: (i[0][5:],i[1]),filter(lambda i: i[0].startswith('app_' ), kwargs.items())))
#	self.db_p = dict(map(lambda i: (i[0][5:],i[1]),filter(lambda i: i[0].startswith('db_' ), kwargs.items())))
#	kwargs = dict(filter(lambda i: not (i[0].startswith('app_') or i[0].startswith('db_')), kwargs.items()))
#	super(Field,self).__init__(*args, **kwargs)
#	#(Field,self).__init__(db_blank,db_null,app_null,app_blank, *args, **kwargs)



class CharField(models.CharField):
    def __init__(self, verbose_name=None, name=None,max_length=512,advanced_help_text=None, *args, **kwargs):
        self.app_p = {'null':True, 'blank':True, 'max_length':max_length}
        self.db_p = {'null':True, 'blank':True, 'max_length':max_length*3}
	self.app_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('app_' ), kwargs.items()))))
	self.db_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('db_' ) and i[0] not in ["db_index", "db_table", "db_column"], kwargs.items()))))

	kwargs = dict(filter(lambda i: not (i[0].startswith('app_') or i[0].startswith('db_')), kwargs.items()))
        kwargs.update(self.db_p)
        if ('primary_key' in kwargs.keys()):
            kwargs['null']=False
        self.app_validators=[]
        self.db_validators=[]
        self.app_validators.append(validators.MaxLengthValidator(self.app_p["max_length"]))
        self.db_validators.append(validators.MaxLengthValidator(self.db_p["max_length"]))
        self.advanced_help_text=advanced_help_text    
            
	super(CharField,self).__init__(verbose_name, name, *args, **kwargs)

    def run_db_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.db_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)

    def run_app_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.app_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)



    def deconstruct(self):
        x,a,b,c=super(CharField,self).deconstruct()
        c['max_length']=self.app_p['max_length']
        return x,a,b,c
        
    def app_validate(self, value, model_instance):
      if value is None and not self.app_p['null']:
        raise exceptions.ValidationError(self.error_messages['null'])
      if not self.app_p['null'] and value in validators.EMPTY_VALUES:
        raise exceptions.ValidationError(self.error_messages['blank'])      
      if self._choices and value not in validators.EMPTY_VALUES:
	    for option_key, option_value in self.choices:
		if isinstance(option_value, (list, tuple)):
		    # This is an optgroup, so look inside the group for
		    # options.
		    for optgroup_key, optgroup_value in option_value:
			if value == optgroup_key:
			    return
		elif value == option_key:
		    return
	    msg = self.error_messages['invalid_choice'] % value
	    raise exceptions.ValidationError(msg)
      self.run_app_validators(value)

    def db_validate(self,value,model_instance):
      if value is None and not self.db_p['null']:
	raise exceptions.ValidationError(self.error_messages['null'])
      if not self.db_p['null'] and value in validators.EMPTY_VALUES:
	raise exceptions.ValidationError(self.error_messages['blank'])
      self.run_db_validators(value)

    def validate(self, value, model_instance):
	"""
	Validates value and throws ValidationError. Subclasses should override
	this to provide validation logic.
	"""
	if not self.editable:
	    # Skip validation for non-editable fields.
	    return

	#validate = self.app_validate(self, value, model_instance)
	return self.db_validate(value, model_instance)	##print "DB STUFF", self.get_attname_column()
#models.CharField = CharField


class TextField(models.TextField):
    def __init__(self, verbose_name=None, name=None,max_length=512,advanced_help_text=None, *args, **kwargs):
        self.app_p = {'null':True, 'blank':True, 'max_length':max_length}
        self.db_p = {'null':True, 'blank':True, 'max_length':max_length*3}
        self.app_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('app_' ), kwargs.items()))))
        self.db_p.update(dict(map(lambda i: (i[0][3:],i[1]),filter(lambda i: i[0].startswith('db_' ) and i[0] not in ["db_index", "db_table", "db_column"], kwargs.items()))))
        self.app_validators=[]
        self.db_validators=[]

        kwargs = dict(filter(lambda i: not (i[0].startswith('app_') or i[0].startswith('db_')), kwargs.items()))
        kwargs.update(self.db_p)
        if ('primary_key' in kwargs.keys()):
            kwargs['null']=False
        if "advanced_help_text" in kwargs:
            del kwargs["advanced_help_text"]
            
        self.app_validators.append(validators.MaxLengthValidator(self.app_p["max_length"]))
        self.db_validators.append(validators.MaxLengthValidator(self.db_p["max_length"]))
        
        super(TextField,self).__init__(verbose_name, name, *args, **kwargs)

    def deconstruct(self):
        x,a,b,c=super(TextField,self).deconstruct()
        c['max_length']=self.app_p['max_length']
        return x,a,b,c        
        
    def run_db_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.db_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)

    def run_app_validators(self, value):
        if value in validators.EMPTY_VALUES:
            return

        errors = []
        for v in self.app_validators:
            try:
                v(value)
            except exceptions.ValidationError as e:
                if hasattr(e, 'code') and e.code in self.error_messages:
                    message = self.error_messages[e.code]
                    if e.params:
                        message = message % e.params
                    errors.append(message)
                else:
                    errors.extend(e.messages)
        if errors:
            raise exceptions.ValidationError(errors)


    def app_validate(self, value, model_instance):
      #if len(value) >= self.app_p['max_length']:
      #  raise exceptions.ValidationError(self.error_messages['max_length'])
      if value is None and not self.app_p['null']:
        raise exceptions.ValidationError(self.error_messages['null'])
      if not self.app_p['null'] and value in validators.EMPTY_VALUES:
        raise exceptions.ValidationError(self.error_messages['blank'])      
      self.run_app_validators(value)
      if self._choices and value not in validators.EMPTY_VALUES:
            for option_key, option_value in self.choices:
                if isinstance(option_value, (list, tuple)):
                    # This is an optgroup, so look inside the group for
                    # options.
                    for optgroup_key, optgroup_value in option_value:
                        if value == optgroup_key:
                            return
                elif value == option_key:
                    return
            msg = self.error_messages['invalid_choice'] % value
            raise exceptions.ValidationError(msg)

    def db_validate(self,value,model_instance):
      #if len(value) >= self.db_p['max_length']:
      #  raise exceptions.ValidationError(self.error_messages['max_length'])      
      if value is None and not self.db_p['null']:
        raise exceptions.ValidationError(self.error_messages['null'])
      if not self.db_p['null'] and value in validators.EMPTY_VALUES:
        raise exceptions.ValidationError(self.error_messages['blank'])
      self.run_db_validators(value)      

    def validate(self, value, model_instance):
        """
        Validates value and throws ValidationError. Subclasses should override
        this to provide validation logic.
        """
        if not self.editable:
            # Skip validation for non-editable fields.
            return

        #validate = self.app_validate(self, value, model_instance)
        return self.db_validate(value, model_instance)  ##print "DB STUFF", self.get_attname_column()
#models.CharField = CharField



#class ForeignKey()

try:
    from south.modelsinspector import add_introspection_rules
    add_introspection_rules([], ["^wiocore\.lib\.dbfields\.CharField"])      
except ImportError:
    pass



#class TextField(models.CharField):
    #def validate(self, value, model_instance):
	#"""
	#Validates value and throws ValidationError. Subclasses should override
	#this to provide validation logic.
	#"""
	#print "Printing validate"
	#if not self.editable:
	    ## Skip validation for non-editable fields.
	    #return

	#if self._choices and value not in validators.EMPTY_VALUES:
	    #for option_key, option_value in self.choices:
		#if isinstance(option_value, (list, tuple)):
		    ## This is an optgroup, so look inside the group for
		    ## options.
		    #for optgroup_key, optgroup_value in option_value:
			#if value == optgroup_key:
			    #return
		#elif value == option_key:
		    #return
	    #msg = self.error_messages['invalid_choice'] % value
	    #raise exceptions.ValidationError(msg)

	#if value is None and not self.null:
	    #raise exceptions.ValidationError(self.error_messages['null'])

	#if not self.blank and value in validators.EMPTY_VALUES:
	    #raise exceptions.ValidationError(self.error_messages['blank'])

    #def clean(self, value, model_instance):
	#"""
	#Convert the value's type and run validation. Validation errors
	#from to_python and validate are propagated. The correct value is
	#returned if no error is raised.
	#"""
	#print "Printing clean"
	#value = self.to_python(value)
	#self.validate(value, model_instance)
	#self.run_validators(value)
	#return value
##models.TextField = TextField

#class DateField(Field):
    #empty_strings_allowed = False
    #default_error_messages = {
        #'invalid': _("'%s' value has an invalid date format. It must be "
                     #"in YYYY-MM-DD format."),
        #'invalid_date': _("'%s' value has the correct format (YYYY-MM-DD) "
                          #"but it is an invalid date."),
    #}
    #description = _("Date (without time)")

    #def __init__(self, verbose_name=None, name=None, auto_now=False,
                 #auto_now_add=False, **kwargs):
        #self.auto_now, self.auto_now_add = auto_now, auto_now_add
        #if auto_now or auto_now_add:
            #kwargs['editable'] = False
            #kwargs['blank'] = True
        #Field.__init__(self, verbose_name, name, **kwargs)

    #def get_internal_type(self):
        #return "DateField"

    #def to_python(self, value):
        #if value is None:
            #return value
        #if isinstance(value, datetime.datetime):
            #if settings.USE_TZ and timezone.is_aware(value):
                ## Convert aware datetimes to the default time zone
                ## before casting them to dates (#17742).
                #default_timezone = timezone.get_default_timezone()
                #value = timezone.make_naive(value, default_timezone)
            #return value.date()
        #if isinstance(value, datetime.date):
            #return value

        #try:
            #parsed = parse_date(value)
            #if parsed is not None:
                #return parsed
        #except ValueError:
            #msg = self.error_messages['invalid_date'] % value
            #raise exceptions.ValidationError(msg)

        #msg = self.error_messages['invalid'] % value
        #raise exceptions.ValidationError(msg)

    #def pre_save(self, model_instance, add):
        #if self.auto_now or (self.auto_now_add and add):
            #value = datetime.date.today()
            #setattr(model_instance, self.attname, value)
            #return value
        #else:
            #return super(DateField, self).pre_save(model_instance, add)

    #def contribute_to_class(self, cls, name):
        #super(DateField,self).contribute_to_class(cls, name)
        #if not self.null:
            #setattr(cls, 'get_next_by_%s' % self.name,
                #curry(cls._get_next_or_previous_by_FIELD, field=self,
                      #is_next=True))
            #setattr(cls, 'get_previous_by_%s' % self.name,
                #curry(cls._get_next_or_previous_by_FIELD, field=self,
                      #is_next=False))

    #def get_prep_lookup(self, lookup_type, value):
        ## For "__month", "__day", and "__week_day" lookups, convert the value
        ## to an int so the database backend always sees a consistent type.
        #if lookup_type in ('month', 'day', 'week_day'):
            #return int(value)
        #return super(DateField, self).get_prep_lookup(lookup_type, value)

    #def get_prep_value(self, value):
        #return self.to_python(value)

    #def get_db_prep_value(self, value, connection, prepared=False):
        ## Casts dates into the format expected by the backend
        #if not prepared:
            #value = self.get_prep_value(value)
        #return connection.ops.value_to_db_date(value)

    #def value_to_string(self, obj):
        #val = self._get_val_from_obj(obj)
        #return '' if val is None else val.isoformat()

    #def formfield(self, **kwargs):
        #defaults = {'form_class': forms.DateField}
        #defaults.update(kwargs)
        #return super(DateField, self).formfield(**defaults)

#class DateTimeField(DateField):
    #empty_strings_allowed = False
    #default_error_messages = {
        #'invalid': _("'%s' value has an invalid format. It must be in "
                     #"YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ] format."),
        #'invalid_date': _("'%s' value has the correct format "
                          #"(YYYY-MM-DD) but it is an invalid date."),
        #'invalid_datetime': _("'%s' value has the correct format "
                              #"(YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]) "
                              #"but it is an invalid date/time."),
    #}
    #description = _("Date (with time)")

    ## __init__ is inherited from DateField

    #def get_internal_type(self):
        #return "DateTimeField"

    #def to_python(self, value):
        #if value is None:
            #return value
        #if isinstance(value, datetime.datetime):
            #return value
        #if isinstance(value, datetime.date):
            #value = datetime.datetime(value.year, value.month, value.day)
            #if settings.USE_TZ:
                ## For backwards compatibility, interpret naive datetimes in
                ## local time. This won't work during DST change, but we can't
                ## do much about it, so we let the exceptions percolate up the
                ## call stack.
                #warnings.warn("DateTimeField received a naive datetime (%s)"
                              #" while time zone support is active." % value,
                              #RuntimeWarning)
                #default_timezone = timezone.get_default_timezone()
                #value = timezone.make_aware(value, default_timezone)
            #return value

        #try:
            #parsed = parse_datetime(value)
            #if parsed is not None:
                #return parsed
        #except ValueError:
            #msg = self.error_messages['invalid_datetime'] % value
            #raise exceptions.ValidationError(msg)

        #try:
            #parsed = parse_date(value)
            #if parsed is not None:
                #return datetime.datetime(parsed.year, parsed.month, parsed.day)
        #except ValueError:
            #msg = self.error_messages['invalid_date'] % value
            #raise exceptions.ValidationError(msg)

        #msg = self.error_messages['invalid'] % value
        #raise exceptions.ValidationError(msg)

    #def pre_save(self, model_instance, add):
        #if self.auto_now or (self.auto_now_add and add):
            #value = timezone.now()
            #setattr(model_instance, self.attname, value)
            #return value
        #else:
            #return super(DateTimeField, self).pre_save(model_instance, add)

    ## contribute_to_class is inherited from DateField, it registers
    ## get_next_by_FOO and get_prev_by_FOO

    ## get_prep_lookup is inherited from DateField

    #def get_prep_value(self, value):
        #value = self.to_python(value)
        #if value is not None and settings.USE_TZ and timezone.is_naive(value):
            ## For backwards compatibility, interpret naive datetimes in local
            ## time. This won't work during DST change, but we can't do much
            ## about it, so we let the exceptions percolate up the call stack.
            #warnings.warn("DateTimeField received a naive datetime (%s)"
                          #" while time zone support is active." % value,
                          #RuntimeWarning)
            #default_timezone = timezone.get_default_timezone()
            #value = timezone.make_aware(value, default_timezone)
        #return value

    #def get_db_prep_value(self, value, connection, prepared=False):
        ## Casts datetimes into the format expected by the backend
        #if not prepared:
            #value = self.get_prep_value(value)
        #return connection.ops.value_to_db_datetime(value)

    #def value_to_string(self, obj):
        #val = self._get_val_from_obj(obj)
        #return '' if val is None else val.isoformat()

    #def formfield(self, **kwargs):
        #defaults = {'form_class': forms.DateTimeField}
        #defaults.update(kwargs)
        #return super(DateTimeField, self).formfield(**defaults)

#class DecimalField(Field):
    #empty_strings_allowed = False
    #default_error_messages = {
        #'invalid': _("'%s' value must be a decimal number."),
    #}
    #description = _("Decimal number")

    #def __init__(self, verbose_name=None, name=None, max_digits=None,
                 #decimal_places=None, **kwargs):
        #self.max_digits, self.decimal_places = max_digits, decimal_places
        #Field.__init__(self, verbose_name, name, **kwargs)

    #def get_internal_type(self):
        #return "DecimalField"

    #def to_python(self, value):
        #if value is None:
            #return value
        #try:
            #return decimal.Decimal(value)
        #except decimal.InvalidOperation:
            #msg = self.error_messages['invalid'] % value
            #raise exceptions.ValidationError(msg)

    #def _format(self, value):
        #if isinstance(value, six.string_types) or value is None:
            #return value
        #else:
            #return self.format_number(value)

    #def format_number(self, value):
        #"""
        #Formats a number into a string with the requisite number of digits and
        #decimal places.
        #"""
        ## Method moved to django.db.backends.util.
        ##
        ## It is preserved because it is used by the oracle backend
        ## (django.db.backends.oracle.query), and also for
        ## backwards-compatibility with any external code which may have used
        ## this method.
        #from django.db.backends import util
        #return util.format_number(value, self.max_digits, self.decimal_places)

    #def get_db_prep_save(self, value, connection):
        #return connection.ops.value_to_db_decimal(self.to_python(value),
                #self.max_digits, self.decimal_places)

    #def get_prep_value(self, value):
        #return self.to_python(value)

    #def formfield(self, **kwargs):
        #defaults = {
            #'max_digits': self.max_digits,
            #'decimal_places': self.decimal_places,
            #'form_class': forms.DecimalField,
        #}
        #defaults.update(kwargs)
        #return super(DecimalField, self).formfield(**defaults)

#class EmailField(CharField):
    #default_validators = [validators.validate_email]
    #description = _("Email address")

    #def __init__(self, *args, **kwargs):
        ## max_length should be overridden to 254 characters to be fully
        ## compliant with RFCs 3696 and 5321

        #kwargs['max_length'] = kwargs.get('max_length', 75)
        #CharField.__init__(self, *args, **kwargs)

    #def formfield(self, **kwargs):
        ## As with CharField, this will cause email validation to be performed
        ## twice.
        #defaults = {
            #'form_class': forms.EmailField,
        #}
        #defaults.update(kwargs)
        #return super(EmailField, self).formfield(**defaults)

#class FilePathField(Field):
    #description = _("File path")

    #def __init__(self, verbose_name=None, name=None, path='', match=None,
                 #recursive=False, allow_files=True, allow_folders=False, **kwargs):
        #self.path, self.match, self.recursive = path, match, recursive
        #self.allow_files, self.allow_folders =  allow_files, allow_folders
        #kwargs['max_length'] = kwargs.get('max_length', 100)
        #Field.__init__(self, verbose_name, name, **kwargs)

    #def formfield(self, **kwargs):
        #defaults = {
            #'path': self.path,
            #'match': self.match,
            #'recursive': self.recursive,
            #'form_class': forms.FilePathField,
            #'allow_files': self.allow_files,
            #'allow_folders': self.allow_folders,
        #}
        #defaults.update(kwargs)
        #return super(FilePathField, self).formfield(**defaults)

    #def get_internal_type(self):
        #return "FilePathField"

#class IntegerField(Field):
    #empty_strings_allowed = False
    #default_error_messages = {
        #'invalid': _("'%s' value must be an integer."),
    #}
    #description = _("Integer")

    #def get_prep_value(self, value):
        #if value is None:
            #return None
        #return int(value)

    #def get_prep_lookup(self, lookup_type, value):
        #if ((lookup_type == 'gte' or lookup_type == 'lt')
            #and isinstance(value, float)):
            #value = math.ceil(value)
        #return super(IntegerField, self).get_prep_lookup(lookup_type, value)

    #def get_internal_type(self):
        #return "IntegerField"

    #def to_python(self, value):
        #if value is None:
            #return value
        #try:
            #return int(value)
        #except (TypeError, ValueError):
            #msg = self.error_messages['invalid'] % value
            #raise exceptions.ValidationError(msg)

    #def formfield(self, **kwargs):
        #defaults = {'form_class': forms.IntegerField}
        #defaults.update(kwargs)
        #return super(IntegerField, self).formfield(**defaults)

#class IPAddressField(Field):
    #empty_strings_allowed = False
    #description = _("IPv4 address")

    #def __init__(self, *args, **kwargs):
        #kwargs['max_length'] = 15
        #Field.__init__(self, *args, **kwargs)

    #def get_internal_type(self):
        #return "IPAddressField"

    #def formfield(self, **kwargs):
        #defaults = {'form_class': forms.IPAddressField}
        #defaults.update(kwargs)
        #return super(IPAddressField, self).formfield(**defaults)

#class TextField(Field):
    #description = _("Text")

    #def get_internal_type(self):
        #return "TextField"

    #def get_prep_value(self, value):
        #if isinstance(value, six.string_types) or value is None:
            #return value
        #return smart_text(value)

    #def formfield(self, **kwargs):
        #defaults = {'widget': forms.Textarea}
        #defaults.update(kwargs)
        #return super(TextField, self).formfield(**defaults)

#class URLField(CharField):
    #description = _("URL")

    #def __init__(self, verbose_name=None, name=None, **kwargs):
        #kwargs['max_length'] = kwargs.get('max_length', 200)
        #CharField.__init__(self, verbose_name, name, **kwargs)
        #self.validators.append(validators.URLValidator())

    #def formfield(self, **kwargs):
        ## As with CharField, this will cause URL validation to be performed
        ## twice.
        #defaults = {
            #'form_class': forms.URLField,
        #}
        #defaults.update(kwargs)
        #return super(URLField, self).formfield(**defaults)
