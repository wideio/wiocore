from static_precompiler.exceptions import StaticCompilationError
from static_precompiler.compilers.base import BaseCompiler
from static_precompiler.settings import MML_EXECUTABLE
from static_precompiler.utils import run_command
import os, settings


class MML(BaseCompiler):
    def is_supported(self, source_path):
        return source_path.endswith(".htmml")

    def get_output_path(self, source_path):
        source_dir = os.path.dirname(source_path)
        source_filename = os.path.basename(source_path)
        output_filename = self.get_output_filename(source_filename)
        return os.path.join( source_dir, output_filename)

    def get_output_filename(self, source_filename):
        return source_filename[:-6] + ".html"

    def compile_file(self, source_path):
        return self.compile_source(self.get_source(source_path))

    def compile_source(self, source):
        args = [
            MML_EXECUTABLE,
#            "-c",
            "-m", "raw",
            "-I", os.path.join(settings.CWD,"mml"),
            "-I", os.path.join(settings.CWD,"../wiocore/mml"),
            "-i", "wiocore.mml",
            "-i", "wioproject.mml"            
        ]
        out, errors = run_command(args, source)
        #if errors:
        #    print "ERRORS"
        #    raise StaticCompilationError(errors)

        return out
