from static_precompiler.exceptions import StaticCompilationError
from static_precompiler.compilers.base import BaseCompiler
from static_precompiler.settings import RAPYDSCRIPT_EXECUTABLE
from static_precompiler.utils import run_command


class RapydScript(BaseCompiler):

    def is_supported(self, source_path):
        return source_path.endswith(".pyj")

    def get_output_filename(self, source_filename):
        return source_filename[:-5] + ".js"

    def compile_file(self, source_path):
        return self.compile_source(self.get_source(source_path))

    def compile_source(self, source):
        args = [
            RAPYDSCRIPT_EXECUTABLE,
            "-b",
            "--screw-ie8",
            "-m",
        ]
        out, errors = run_command(args, source)
        if errors:
            raise StaticCompilationError(errors)

        return out
