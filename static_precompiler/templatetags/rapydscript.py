from django.template.base import Library
from static_precompiler.compilers.rapydscript import RapydScript
from static_precompiler.templatetags.base import BaseInlineNode


register = Library()
compiler = RapydScript()

class InlineRapydscriptNode(BaseInlineNode):
    compiler = compiler

#noinspection PyUnusedLocal
@register.tag(name="inlinerapydscript")
def do_inlinecoffeescript(parser, token):
    nodelist = parser.parse(("endinlinerapydscript",))
    parser.delete_first_token()
    return InlineRapydscriptNode(nodelist)


@register.simple_tag
def rapydcript(path):
    return compiler.compile(path)
