# -*- coding: utf-8 -*-
import re
import sys
import os
import inspect
import traceback

reinterpret_on_if=True
reinterpret_on_expand=False # <QUITE SYSTEM SETTING
warning_if_not_found=True
encoding='utf8'


from mml_engine import *

class MmlKeyword:
    is_overridable=False
    protect_until=None
    #context=None

    @classmethod
    def parse_arguments(cls,args,context):
        (fargs, fallargs, fallkwargs, fdefaults)=inspect.getargspec(cls.process)
        fargs=fargs[1:]
        if (fdefaults!=None):
            fdefaults=list(fdefaults)
            #sys.stderr.write(repr( (fdefaults, fargs)))
            d=dict(zip(fargs[-(len(fdefaults)):],fdefaults))
        else:
            d={}
        if fallargs==None:
            fallargs="extraargs"
        ld=parse_arguments_for_kw(cls,args,context,fargs,fallargs)
        for e in ld:
            d[str(e[0])]=e[1]
        return d

    @classmethod
    def get_declared_arguments(cls,args,context):
        ld=parse_arguments_for_kw(cls,args,context,[],"*_declarg")
        return ld

    @classmethod
    def get_declared_arguments_as_dict(cls,args,context):
        d={}
        ld=parse_arguments_kw(cls,args,context,[],"*_declarg")
        for e in ld:
            d[str(e[0])]=e[1]
        return ld


    @classmethod
    def call(cls,args,context,mb):
        d=cls.parse_arguments(args,context)
        #sys.stderr.write(repr(cls)+str(d))
        d['__macroblock__']=mb
        r=cls.process(context,**d)
        if (r==None):
            if (mb !=None and mb.protected and cls.protect_until):
                context.protect_until=cls.protect_until
            return
        context.output(unicode(r))
        if (mb !=None and mb.protected and cls.protect_until):
            context.protect_until=cls.protect_until

class MmlMacro(MmlKeyword):
    name=u"unnamed macro"
    is_overridable=True
    buf=u""
    args=[]
    argsdefaults={}
    protect_until=None

    @classmethod
    def parse_arguments(cls,args,context):
        fargs=cls.args[:]
        #fargs.reverse()
        d=cls.argsdefaults.copy()
        fallargs="*"
        ld=parse_arguments_for_kw(cls,args,context,fargs,fallargs)
        for e in ld:
            d[e[0]]=e[1]
        #print d
        return d

    @classmethod
    def call(cls,args,context,mb):
        if (type(args)==dict) :
	  d=args
	else:
          d=cls.parse_arguments(args,context)
        d["__args"]=args
	d["__args_dict"]=d.copy()
        for k in d.keys():
            context.push_var(k,d[k])
        prev_rel=context.with_relative
        context.with_relative=True
        context.push_interpret(cls.buf,cls.name)
        context.with_relative=prev_rel
        for k in d.keys():
            context.pop_var(k)
        #context.output(unicode(r))

class MmlMacroEB(MmlMacro):
    @classmethod
    def call(cls,args,context,mb):
        d=cls.parse_arguments(args,context)
        context.opened_env.append(cls.tag)
        context.push_output(OutputBuf())
        context.push_call(("USERENV",cls.tag, d))
        if (mb !=None and mb.protected and cls.protect_until):
            context.protect_until=cls.protect_until
        if u"body" in d.keys():
           mmlerror("body is a reserved variable name in enviroments.",context)
        d["__args"]=args
	d["__args_dict"]=d.copy() ## avoid cross 
        for k in d.keys():
            context.push_var(k,d[k])


class MmlMacroEE(MmlMacro):
    @classmethod
    def call(cls,args,context,mb):
        if (context.protect_until):
            context.protect_until=None
        #print topstack
        _topstack=context.callstack[-1]
        topstack=_topstack[0]
        if (topstack[0]!="USERENV"):
            mmlerror(" Top of callstack should be USERENV but it is " + str(topstack[0])+"\n", context)
        if (topstack[1]!=cls.tag):
            mmlerror("Opened user enviroment was "+topstack[1]+ " but it is closed by a "+ cls.tag, context)
        _topstack=context._pop_call()           
        topstack=_topstack[0]        
        d=topstack[2]
	xd={}
        d[u"body"]=xd[u"body"]=context.pop_output().get()
        #sys.stderr.write( "body"+str(len(d["body"])))
        for k in xd.keys():
            context.push_var(k,d[k])
        prev_rel=context.with_relative
        context.with_relative=True
        context.push_interpret(cls.buf,cls.name)
        context.with_relative=prev_rel
        for k in d.keys():
            context.pop_var(k)
        context.opened_env.pop(-1)
