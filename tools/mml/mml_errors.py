# -*- coding: utf-8 -*-

import sys
import traceback

reinterpret_on_if=True
reinterpret_on_expand=True
warning_if_not_found=True
encoding='utf8'

try:
  import termcolor
  colored=termcolor.colored
except:
  def colored(x,*args):
      return x


def mmlwarning(msg,context=None,with_stack=None):
   if (with_stack==None) and (context!=None):
      with_stack=context.show_mml_trace_on_warning
   if (type(msg)!=unicode):
      msg=unicode(msg)
   if context:
     if context.display_warnings:
        sys.stderr.write(colored("MML WARNING : "+msg.encode('utf8'),'blue','on_white',['bold'])+"\n")
   else:
      sys.stderr.write(colored("MML WARNING : "+msg.encode('utf8'),'blue','on_white',['bold'])+"\n")
   if context and context.display_warnings:
     if (with_stack):
       sys.stderr.write("MML TRACE:\n\n")
       for r in context.readers:
         try:
           sys.stderr.write(" -"+str(r[0].get_bufname())+" at line "+str(r[0].get_lineno())+": %r\n"%(r[1].split("\n")[r[0].get_lineno()-1]))
         except:
           sys.stderr.write((" - /!\\ Internal Error while traceback for reader : %r"%(r,))+"\n") 
     if context.show_callstack_on_warning:
       sys.stderr.write("MML CALL TRACE:\n\n")
       for r in context.callstack:
         if len(str(r))<=80:
           sys.stderr.write(" -"+str(r)+"\n")
         else:
           sys.stderr.write(" -"+str(r)[:40]+"(...)"+str(r)[-40:]+"\n")
       sys.stderr.write("/STACK\n\n")
   #else: 
   #  sys.stderr.write("NO CONTEXT INFO ASSOCIATED TO THIS WARNING\n\n")

def mmlerror(msg,context):
   if (type(msg)!=unicode):
      msg=unicode(msg)
   sys.stderr.write(colored("MML ERROR : "+msg.encode('utf8'),'red','on_white',['bold'])+"\n")
   sys.stderr.write(colored("MML BUFFER TRACE:\n",'red','on_white',['bold'])+"\n")
   for r in context.readers:
       sys.stderr.write(colored(" -"+str(r[0].get_bufname())+" at line "+str(r[0].get_lineno()),'red','on_white',['bold'])+"\n")
   sys.stderr.write(colored("MML CALL TRACE:\n",'red','on_white',['bold'])+"\n")
   for r in context.callstack:
       sys.stderr.write(colored(" -"+str(r),'red','on_white',['bold'])+"\n")
   if (hasattr(sys,"last_traceback")):
		  traceback.print_tb(sys.last_traceback)
   else:
		  traceback.print_tb(sys.exc_traceback)
       
   sys.exit(-1)
   raise Exception, "Error"


def mmlassert(cond, context):
  if (not cond):
     mmlerror("MML assert failed : "+str(cond),context)
