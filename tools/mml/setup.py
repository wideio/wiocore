# -*- coding: utf-8 -*-
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("mml_engine",     ["mml_engine.pyx"], extra_compile_args=["-DNDEBUG", "-pthread" ,"-fPIC","-fno-strict-aliasing", "-fomit-frame-pointer","-O6","-mmmx"]),
                    ]
)
